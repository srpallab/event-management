import 'package:objectbox/objectbox.dart';

@Entity()
class OwnerModel {
  @Id()
  int id;

  OwnerModel({this.id = 0});
}
