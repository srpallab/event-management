import 'package:objectbox/objectbox.dart';

@Entity()
class TaskModel {
  @Id()
  int id;

  String text;
  bool status;

  TaskModel(this.text, {this.id = 0, this.status = false});

  bool setFinished() {
    status = !status;
    return status;
  }
}
